﻿using Life.Game.LifeFormEntity;

namespace LifeTests
{
    public class Authentication
    {
        public bool Authenticate(Human human, string password)
        {
            // Logic to check password
            IsAuthenticated = password == password;
            human.SetAuthToken(password);
            return true;
        }

        public bool IsAuthenticated { get; set; }
    }
}