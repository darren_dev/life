﻿using Life.Game.LifeFormEntity;
using Life.Game.MechanicalEntity;
using Xunit;

namespace LifeTests.CarTests
{
    public class CarCreateTests
    {
        [Fact]
        public void CreateCar_ShouldCreateCar()
        {
            Car car = new Car();

            Assert.NotNull(car);
            Assert.False(car.HasOwner());
        }

        [Fact]
        public void CreateCar_SetOwner_ShouldHaveOwner()
        {
            Car car = new Car();
            Human human = new Human();

            car.SetOwner(human);

            Assert.NotNull(car.GetOwner());
            Assert.True(car.HasOwner());
        }
    }
}
