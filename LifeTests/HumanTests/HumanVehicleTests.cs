﻿using Life.Game.LifeFormEntity;
using Life.Game.MechanicalEntity;
using Xunit;

namespace LifeTests.HumanTests
{
    public class HumanVehicleTests
    {
        [Fact]
        public void HumanClaim_Car_ShouldHaveCar()
        {
            Human human = new Human();
            Car car = new Car();

            human.AddMechanical(car);

            Assert.NotEmpty(human.GetMechanicals());
        }

        [Fact]
        public void HumanClaimMultiple_Car_ShouldHaveMultipleCar()
        {
            Human human = new Human();
            Car car = new Car();

            human.AddMechanical(car);
            human.AddMechanical(car);
            human.AddMechanical(car);

            Assert.Equal(3, human.GetMechanicals().Count);
        }
    }
}
