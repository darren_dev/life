﻿using System.Linq;
using Life.Enums;
using Life.Game.ItemEntity;
using Life.Game.LifeFormEntity;
using Xunit;

namespace LifeTests.HumanTests
{
    public class HumanInventoryTests
    {
        [Fact]
        public void Human_GetItem_ShouldHaveItem()
        {
            Human human = new Human();
            Shield shield = new Shield();

            human.AddItemToInventory(shield);

            Assert.NotEmpty(human.GetInventory());
        }

        [Fact]
        public void Human_EquipItem_ShouldEquipItem()
        {
            Human human = new Human();
            Shield shield = new Shield();

            var equipmentId = human.QuickEquipItem(shield, EquipLocation.Hand);
            var item = human.GetItemFromEquipmentLocation(equipmentId);

            Assert.Same(shield, item);
        }

        [Fact]
        public void Human_UnequipItem_ShouldUnequipItem()
        {
            Human human = new Human();
            Shield shield = new Shield();

            var equipmentId = human.QuickEquipItem(shield, EquipLocation.Hand);
            human.UnequipItem(equipmentId);
            var item = human.GetItemFromEquipmentLocation(equipmentId);
            var inventoryItem = human.GetInventory().First();

            Assert.Null(item);
            Assert.Same(shield, inventoryItem);
        }

        [Fact]
        public void Human_DropItem_ShouldDropItem_IntoPlace()
        {
            Human human = new Human();
            Shield shield = new Shield();

            var index = human.AddItemToInventory(shield);

            Assert.NotEmpty(human.GetInventory());

            human.DropItemFromInventory(index);
            
            Assert.Same(shield, human.GetPlace().GetInventory()[0]);
            Assert.Empty(human.GetInventory());
        }
    }
}
