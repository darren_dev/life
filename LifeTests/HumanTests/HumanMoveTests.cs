using Life.Enums;
using Life.Game.LifeFormEntity;
using Xunit;

namespace LifeTests.HumanTests
{
    public class HumanMoveTests
    {
        [Fact]
        public void HumanMove_ShouldMove()
        {
            Human human = new Human();

            human.Move(1, Direction.South);

            Assert.Equal(1, human.GetSpacesMoved());
            Assert.Equal(Direction.South, human.GetDirectionMoved());
        }

        [Fact]
        public void HumanMoveTwice_ShouldMoveTwice()
        {
            Human human = new Human();

            human.Move(1, Direction.South);
            human.Move(1, Direction.South);

            Assert.Equal(2, human.GetSpacesMoved());
            Assert.Equal(Direction.South, human.GetDirectionMoved());
        }
    }
}