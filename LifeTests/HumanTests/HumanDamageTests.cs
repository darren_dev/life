using Life.Game.LifeFormEntity;
using Xunit;

namespace LifeTests.HumanTests
{
    public class HumanDamageTests
    {
        [Fact]
        public void HumanDamage_Human_ShouldReduceHealth()
        {
            Human attacker = new Human();
            Human defender = new Human();

            attacker.Attack(defender);

            Assert.Equal(99, defender.GetHealth());
        }

        [Fact]
        public void MultipleHumanDamage_Human_ShouldReduceHealth()
        {
            Human attacker = new Human();
            Human attackerTwo = new Human();

            Human defender = new Human();

            attackerTwo.Attack(defender);
            attacker.Attack(defender);

            Assert.Equal(98, defender.GetHealth());
        }

        [Fact]
        public void HumanDamageWithMultiplier_Human_ShouldReduceHealth()
        {
            Human attacker = new Human();
            Human defender = new Human();

            attacker.AddDamageMultiplier(2);
            attacker.Attack(defender);

            Assert.Equal(98, defender.GetHealth());
        }

        [Fact]
        public void HumanDamage_Alien_ShouldReduceHealth()
        {
            Human attacker = new Human();
            Alien defender = new Alien();

            attacker.Attack(defender);

            Assert.Equal(199.5, defender.GetHealth());
        }

        [Fact]
        public void HumanDamageWithMultiplier_Alien_ShouldReduceHealth()
        {
            var attacker = new Human();
            Alien defender = new Alien();

            attacker.AddDamageMultiplier(2);
            attacker.Attack(defender);

            Assert.Equal(199, defender.GetHealth());
        }

        [Fact]
        public void HumanDamage_HumanHeal_ShouldAddHealth()
        {
            Human human = new Human();
            human.SetHealth(10);

            human.AddHealth(90);

            Assert.Equal(100, human.GetHealth());
        }
    }
}