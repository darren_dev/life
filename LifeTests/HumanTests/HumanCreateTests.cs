﻿using Life.Enums;
using Life.Game;
using Life.Game.LifeFormEntity;
using Xunit;

namespace LifeTests.HumanTests
{
    public class HumanCreateTests
    {
        [Fact]
        public void CreateHuman_ShouldCreateHuman()
        {
            Human human = new Human();

            Assert.Equal(100, human.GetHealth());
            Assert.Equal(1, human.GetDamageWithModifiers());
            Assert.Equal(DefenceType.Human, human.GetDefenceType());
            Assert.Same(human.GetPlace(), GameController.Instance.Park);
        }
        

        [Fact]
        public void CreateHuman_SetHumanDetails_ShouldContainDetails()
        {
            Human human = new Human();
            human.SetFirstName("Darren");
            human.SetLastName("Whitfield");
            human.SetAge(25);
            human.SetGender(Gender.Male);

            Assert.NotNull(human.FirstName);
            Assert.NotNull(human.LastName);
            Assert.NotNull(human.Gender);
            Assert.Equal("Darren Whitfield", human.GetCallingName());
        }
    }
}