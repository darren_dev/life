﻿using Life.Game.Base;
using Life.Game.LifeFormEntity;
using Xunit;

namespace LifeTests.HumanTests
{
    public class HumanInsertionTests
    {
        [Fact]
        public void Human_InsertIntoPlace_ShouldBeInPlace()
        {
            Human janine = new Human();
            Place castle = new Place();

            castle.AddLifeForm(janine);

            var hasLifeForm = castle.HasLifeForm(janine);
            Assert.True(hasLifeForm);
        }
    }
}
