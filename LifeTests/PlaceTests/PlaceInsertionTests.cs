﻿using Life.Game.Base;
using Life.Game.LifeFormEntity;
using Life.Game.PlaceEntity;
using Xunit;

namespace LifeTests.PlaceTests
{
    public class PlaceInsertionTests
    {
        [Fact]
        public void Place_AddLifeForm_AddsLifeForm()
        {
            Place park = new Park();
            LifeForm human = new Human();
            LifeForm alien = new Alien();

            park.AddLifeForm(human);
            park.AddLifeForm(alien);

            Assert.NotEmpty(park.GetAllLifeForms());
            Assert.Same(park, human.GetPlace());
            Assert.Same(park, alien.GetPlace());
        }

        [Fact]
        public void Reference_Test()
        {
            Place park = new Park();
            park.SetName("a");

            Assert.Equal("a", park.GetName());

            LifeForm human = new Human();
            LifeForm alien = new Alien();

            park.AddLifeForm(human);
            park.AddLifeForm(alien);

            Assert.NotEmpty(park.GetAllLifeForms());
            Assert.Same(park, human.GetPlace());
            Assert.Same(park, alien.GetPlace());

            park.SetName("b");

            Assert.Equal("b", human.GetPlace().GetName());
            Assert.Equal("b", alien.GetPlace().GetName());
        }
    }
}
