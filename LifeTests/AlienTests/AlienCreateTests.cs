﻿using Life.Enums;
using Life.Game;
using Life.Game.LifeFormEntity;
using Xunit;

namespace LifeTests.AlienTests
{
    public class AlienCreateTests
    {
        [Fact]
        public void CreateAlien_ShouldCreateAlien()
        {
            Alien alien = new Alien();

            Assert.NotNull(alien);
            Assert.Equal(200, alien.GetHealth());
            Assert.Equal(1, alien.GetDamageWithModifiers());
            Assert.Equal(DefenceType.Alien, alien.GetDefenceType());
        }

        [Fact]
        public void CreateAlien_SetName_ShouldHaveCallingName()
        {
            Alien alien = new Alien();
            alien.SetName("Alien number 1");

            Assert.Equal("Alien number 1", alien.GetCallingName());
        }
    }
}