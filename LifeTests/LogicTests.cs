﻿using Life;
using Life.Game.LifeFormEntity;
using Xunit;

namespace LifeTests
{
    public class LogicTests
    {

        [Fact]
        public void Assert_AddingNumbers_DoAdd()
        {
            int result = Logic.Add(1, 2);
            Assert.Equal(3, result);
        }

        [Fact]
        public void Assert_HumanMessage_Human_ContainsMessage()
        {
            Human earl = new Human();
            Human darren = new Human();

            earl.Message(darren, "Hello there");

            Assert.NotEmpty(darren.CurrentMessages);
        }

        [Fact]
        public void Asser_HumanLogin_IsValid()
        {
            Human janine = new Human();
            
            Authentication authorizser = new Authentication();
            authorizser.Authenticate(janine, "password");

            Assert.True(authorizser.IsAuthenticated);
            Assert.NotEmpty(janine.AuthToken);

        }
    }
}

