﻿using Life.Game.Base;

namespace Life.Interfaces
{
    public interface IJoinPlace
    {
        void SetPlace(Place place);
        Place GetPlace();
    }
}