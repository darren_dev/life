﻿using Life.Enums;

namespace Life.Interfaces
{
    public interface ICanMove
    {
        void Move(int spaces, Direction direction);
        int GetSpacesMoved();
        Direction GetDirectionMoved();
    }
}