﻿using Life.Enums;

namespace Life.Interfaces
{
    public interface IHaveDefence: IHealth, IArmor
    {
        //DefenceType DefenceType { get; }
        void SetDefenceType(DefenceType defenceType);
        DefenceType GetDefenceType();
    }
}
