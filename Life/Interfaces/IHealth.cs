﻿namespace Life.Interfaces
{
    public interface IHealth
    {
        double GetHealth();
        void SetHealth(double health);
        void AddHealth(double health);
        void RemoveHealth(double health);
    }
}