﻿using Life.Enums;

namespace Life.Interfaces
{
    public interface ICanEquip
    {
        int QuickEquipItem(IItem item, EquipLocation equipLocation);
        void EquipItem(IItem item, int equipmentId);
        void UnequipItem(int equipmentId);
        int AddEquipmentLocation(EquipLocation equipLocation);
        IItem GetItemFromEquipmentLocation(int equipmentId);
    }
}