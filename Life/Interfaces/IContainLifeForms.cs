﻿using System.Collections.Generic;
using Life.Game.Base;

namespace Life.Interfaces
{
    public interface IContainLifeForms
    {
        void AddLifeForm(LifeForm lifeForm);
        List<LifeForm> GetAllLifeForms();
    }
}