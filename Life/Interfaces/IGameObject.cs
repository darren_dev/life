﻿namespace Life.Interfaces
{
    public interface IGameObject
    {
        double X { get; set; }
        double Y { get; set; }
        double Z { get; set; }
    }
}