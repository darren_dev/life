﻿namespace Life.Interfaces
{
    public interface IArmor
    {
        //double CurrentArmor { get; }

        void SetArmor(double armor);
        void AddArmor(double armor);
        void RemoveArmor(double armor);
    }
}
