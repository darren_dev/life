﻿namespace Life.Interfaces
{
    public interface IHaveName
    {
        void SetName(string name);
        string GetName();
    }
}