﻿using System.Collections.Generic;
using Life.Enums;

namespace Life.Interfaces
{
    public interface IHaveDamage
    {
        //double BaseDamageAmount { get; }
        //List<double> DamageMultiplier { get; }
        double GetMultiplierByType(DefenceType defenceType);
        void AddMultiplierType(DefenceType defenceType, double multiplier);
        void Attack(IHaveDefence defender);
        double GetDamageWithModifiers();
        void SetBaseDamage(double damage);
        void AddDamageMultiplier(double multiplier);
    }
}