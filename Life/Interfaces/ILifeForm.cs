﻿using Life.Enums;

namespace Life.Interfaces
{
    public interface ILifeForm
    {
        //int Age { get; }
        //Direction DirectionMoved { get; }
        //int SpacesMoved { get; }
        
        void SetAge(int age);
        string GetCallingName();
    }
}