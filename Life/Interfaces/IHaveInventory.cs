﻿using System.Collections.Generic;

namespace Life.Interfaces
{
    public interface IHaveInventory
    {
        int AddItemToInventory(IItem item);
        IItem GetItemFromInventory(int inventorySlot);
        List<IItem> GetInventory();
        void DropItemFromInventory(int index);
    }
}