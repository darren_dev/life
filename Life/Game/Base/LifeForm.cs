﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Life.Enums;
using Life.Interfaces;

namespace Life.Game.Base
{
    public abstract class LifeForm : GameObject, ILifeForm, IHaveDamage, IHaveDefence, IHaveInventory, ICanEquip, ICanMove, IJoinPlace
    {
        //public int Age => _age;
        //public int SpacesMoved => _spacesMoved;
        //public Direction DirectionMoved => _directionMoved;
        //public double CurrentHealth => _currentHealth;
        //public double CurrentArmor => _currentArmor;
        //public double BaseDamageAmount => _baseDamage;
        //public List<double> DamageMultiplier => _damageMultiplier;
        //public DefenceType DefenceType => _defenceType;

        private int _age;
        private int _spacesMoved;
        private Direction _directionMoved;
        private double _currentHealth;
        private double _currentArmor;
        private double _baseDamage;
        private DefenceType _defenceType;
        private readonly List<double> _damageMultiplier;
        private readonly Dictionary<DefenceType, double> _multipliersByType;
        private readonly List<IItem> _inventory;
        private readonly List<Equipment> _equipmentList;
        private Place _place;

        public abstract string GetCallingName();

        protected LifeForm()
        {
            _damageMultiplier = new List<double>();
            _multipliersByType = new Dictionary<DefenceType, double>();
            _inventory = new List<IItem>();
            _equipmentList = new List<Equipment>();
        }

        public void SetDefenceType(DefenceType defenceType)
        {
            _defenceType = defenceType;
        }

        public DefenceType GetDefenceType()
        {
            return _defenceType;
        }

        public void SetAge(int age)
        {
            _age = age;
        }

        #region Place
        public void SetPlace(Place place)
        {
            _place = place;
        }

        public Place GetPlace()
        {
            return _place;
        }
        #endregion

        #region Health
        public double GetHealth()
        {
            return _currentHealth;
        }

        public void SetHealth(double health)
        {
            _currentHealth = health;
        }

        public void AddHealth(double health)
        {
            _currentHealth = _currentHealth + health;
        }

        public void RemoveHealth(double health)
        {
            _currentHealth = _currentHealth - health;
        }
        #endregion

        #region Armor
        public void SetArmor(double armor)
        {
            _currentArmor = armor;
        }

        public void AddArmor(double armor)
        {
            _currentArmor = _currentArmor + armor;
        }

        public void RemoveArmor(double armor)
        {
            _currentArmor = _currentArmor - armor;
        }
        #endregion

        #region Damage
        public double GetDamageWithModifiers()
        {
            return _baseDamage * AddAllDamageMultipliers();
        }

        private double AddAllDamageMultipliers()
        {
            double result = 1;
            foreach (var d in _damageMultiplier)
            {
                if (d > 0)
                {
                    result = result * d;
                }
            }
            return result;
        }

        public void SetBaseDamage(double damage)
        {
            _baseDamage = damage;
        }

        public void AddDamageMultiplier(double multiplier)
        {
            _damageMultiplier.Add(multiplier);
        }

        public void Attack(IHaveDefence defender)
        {
            var multiplierByType = GetMultiplierByType(defender.GetDefenceType());
            AddDamageMultiplier(multiplierByType);

            defender.RemoveHealth(GetDamageWithModifiers());
        }

        public double GetMultiplierByType(DefenceType defenceType)
        {
            double multiplier;
            _multipliersByType.TryGetValue(defenceType, out multiplier);

            return multiplier;
        }

        public void AddMultiplierType(DefenceType defenceType, double multiplier)
        {
            _multipliersByType.Add(defenceType, multiplier);
        }
        #endregion

        #region Inventory
        public int AddItemToInventory(IItem item)
        {
            _inventory.Add(item);
            return _inventory.Count - 1;
        }

        public IItem GetItemFromInventory(int inventorySlot)
        {
            IItem itemFromInventory = _inventory[inventorySlot];

            return itemFromInventory;
        }

        public List<IItem> GetInventory()
        {
            return _inventory;
        }

        public void DropItemFromInventory(int index)
        {
            var item = GetItemFromInventory(index);
            _inventory.RemoveAt(index);

            GetPlace().AddItemToInventory(item);
        }
        #endregion

        #region Equipment
        public int QuickEquipItem(IItem item, EquipLocation equipLocation)
        {
            var equipLocations = _equipmentList.Where(x => x.EquipmentLocation == equipLocation);

            foreach (var location in equipLocations)
            {
                if (location.Item != null)
                {
                    continue;
                }

                location.Item = item;
                return location.Id;
            }

            return -1;
        }

        public void EquipItem(IItem item, int equipmentId)
        {
            var equipment = _equipmentList.First(x => x.Id == equipmentId);

            AddItemToInventory(equipment.Item);
            equipment.Item = item;
        }

        public void UnequipItem(int equipmentId)
        {
            var equipment = _equipmentList.First(x => x.Id == equipmentId);

            AddItemToInventory(equipment.Item);
            equipment.Item = null;
        }

        public int AddEquipmentLocation(EquipLocation equipLocation)
        {
            var lastEquipmentLocation = _equipmentList.LastOrDefault();
            var id = lastEquipmentLocation?.Id ?? 0;

            _equipmentList.Add(new Equipment
            {
                Id = id + 1,
                EquipmentLocation = equipLocation
            });

            return id;
        }

        public IItem GetItemFromEquipmentLocation(int equipmentId)
        {
            return _equipmentList.FirstOrDefault(x => x.Id == equipmentId)?.Item;
        }

        #endregion

        #region Move
        public void Move(int spaces, Direction direction)
        {
            _spacesMoved = _spacesMoved + spaces;
            _directionMoved = direction;
        }

        public int GetSpacesMoved()
        {
            return _spacesMoved;
        }

        public Direction GetDirectionMoved()
        {
            return _directionMoved;
        }
        #endregion
    }

    public interface IJoinPlace
    {
         void SetPlace(Place place);
        Place GetPlace();
    }
}