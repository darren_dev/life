﻿using Life.Enums;
using Life.Interfaces;

namespace Life.Game.Base
{
    public class Equipment
    {
        public int Id { get; set; }
        public EquipLocation EquipmentLocation { get; set; }
        public IItem Item { get; set; }
    }
}