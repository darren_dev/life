﻿using Life.Interfaces;

namespace Life.Game.Base
{
    public class GameObject : IGameObject
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
    }
}
