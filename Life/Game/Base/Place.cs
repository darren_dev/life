﻿using System.Collections.Generic;
using Life.Interfaces;

namespace Life.Game.Base
{
    public class Place : IHaveInventory, IContainLifeForms, IHaveName
    {
        private readonly List<IItem> _items;
        private string _name;
        private List<LifeForm> _lifeForms;

        public Place()
        {
            _items = new List<IItem>();
            _lifeForms = new List<LifeForm>();
        }

        public int AddItemToInventory(IItem item)
        {
            _items.Add(item);
            return _items.Count - 1;
        }

        public IItem GetItemFromInventory(int inventorySlot)
        {
            return _items[inventorySlot];
        }

        public List<IItem> GetInventory()
        {
            return _items;
        }

        public void DropItemFromInventory(int index)
        {
            _items.RemoveAt(index);
        }

        public void AddLifeForm(LifeForm lifeForm)
        {
            _lifeForms.Add(lifeForm);
            lifeForm.SetPlace(this);
        }

        public List<LifeForm> GetAllLifeForms()
        {
            return _lifeForms;
        }

        public void SetName(string name)
        {
            _name = name;
        }

        public string GetName()
        {
            return _name;
        }

        public bool HasLifeForm(ILifeForm lifeForm)
        {
            return true;
        }
    }
}
