﻿using Life.Interfaces;

namespace Life.Game.Base
{
    public class Item : GameObject, IArmor, IItem
    {
        public double CurrentArmor => _armor;

        private double _armor;

        public void SetArmor(double armor)
        {
            _armor = armor;
        }

        public void AddArmor(double armor)
        {
            _armor = _armor + armor;
        }

        public void RemoveArmor(double armor)
        {
            _armor = _armor - armor;
        }
    }
}