﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using Life.Enums;

namespace Life.Game.LifeFormEntity
{
    public class Human : Humanoid
    {
        public string FirstName => _firstName;
        public string LastName => _lastName;
        public Gender Gender => _gender;

        public IEnumerable CurrentMessages
        {
            get { return messages; }
        }

        public IEnumerable AuthToken
        {
            get { return _authToken; }
        }

        private Gender _gender;
        private string _firstName;
        private string _lastName;

        private List<string> messages;
        private string _authToken;

        public Human()
        {
            SetHealth(100);
            SetBaseDamage(1);
            AddDamageMultiplier(1);
            SetDefenceType(DefenceType.Human);

            AddMultiplierType(DefenceType.Alien, 0.5);

            AddEquipmentLocation(EquipLocation.Head);
            AddEquipmentLocation(EquipLocation.Shoulder);
            AddEquipmentLocation(EquipLocation.Shoulder);
            AddEquipmentLocation(EquipLocation.Arm);
            AddEquipmentLocation(EquipLocation.Arm);
            AddEquipmentLocation(EquipLocation.Hand);
            AddEquipmentLocation(EquipLocation.Hand);
            AddEquipmentLocation(EquipLocation.Finger);
            AddEquipmentLocation(EquipLocation.Finger);
            AddEquipmentLocation(EquipLocation.Finger);
            AddEquipmentLocation(EquipLocation.Torso);
            AddEquipmentLocation(EquipLocation.Waist);
            AddEquipmentLocation(EquipLocation.Leg);
            AddEquipmentLocation(EquipLocation.Foot);
            AddEquipmentLocation(EquipLocation.Foot);

            SetPlace(GameController.Instance.Park);

            messages = new List<string>();
        }

        public override string GetCallingName()
        {
            return _firstName + " " + _lastName;
        }

        public void SetFirstName(string firstName)
        {
            _firstName = firstName;
        }

        public void SetLastName(string lastName)
        {
            _lastName = lastName;
        }

        public void SetGender(Gender gender)
        {
            _gender = gender;
        }

        public void Message(Human human, string message)
        {
            human.AddMessage(message);
        }

        private void AddMessage(string message)
        {
            messages.Add(message);
        }

        public void SetAuthToken(string authToken)
        {
            _authToken = authToken;
        }
    }
}