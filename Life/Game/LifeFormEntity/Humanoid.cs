﻿using System.Collections.Generic;
using Life.Game.Base;

namespace Life.Game.LifeFormEntity
{
    public abstract class Humanoid : LifeForm
    {
        private readonly List<Mechanical> _mechanicals;

        protected Humanoid()
        {
            _mechanicals = new List<Mechanical>();
        }

        public virtual bool CanUseWeapon()
        {
            return false;
        }

        public void AddMechanical(Mechanical mechanical)
        {
            _mechanicals.Add(mechanical);
        }

        public List<Mechanical> GetMechanicals()
        {
            return _mechanicals;
        }
    }
}
