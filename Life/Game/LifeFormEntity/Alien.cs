﻿using Life.Enums;

namespace Life.Game.LifeFormEntity
{
    public class Alien : Humanoid
    {
        private string _name;

        public Alien()
        {
            SetHealth(200);
            SetBaseDamage(1);
            SetDefenceType(DefenceType.Alien);

            AddMultiplierType(DefenceType.Alien, 2);
        }

        public override string GetCallingName()
        {
            return _name;
        }
        
        public void SetName(string name)
        {
            _name = name;
        }
    }
}