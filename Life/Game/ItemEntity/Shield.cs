﻿using Life.Game.Base;

namespace Life.Game.ItemEntity
{
    public class Shield : Item
    {
        public Shield()
        {
            SetArmor(10);
        }
    }
}
