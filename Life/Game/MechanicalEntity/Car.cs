﻿using Life.Game.Base;
using Life.Game.LifeFormEntity;

namespace Life.Game.MechanicalEntity
{
    public class Car : Mechanical
    {
        private Humanoid _owner;

        public void SetOwner(Humanoid humanoid)
        {
            _owner = humanoid;
        }

        public Humanoid GetOwner()
        {
            return _owner;
        }

        public bool HasOwner()
        {
            return _owner != null;
        }
    }
}
