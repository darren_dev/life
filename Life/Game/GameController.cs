﻿using System;
using System.Threading;
using Life.Game.Base;
using Life.Game.PlaceEntity;

namespace Life.Game
{
    public sealed class GameController
    {
        public static GameController Instance => Lazy.Value;
        private static readonly Lazy<GameController> Lazy = new Lazy<GameController>(() => new GameController(), LazyThreadSafetyMode.ExecutionAndPublication);

        public Place Park { get; private set; }

        public GameController()
        {
            Park = new Park();
        }
    }
}
