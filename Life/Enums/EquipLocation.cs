﻿namespace Life.Enums
{
    public enum EquipLocation
    {
        Head,
        Shoulder,
        Arm,
        Hand,
        Finger,
        Torso,
        Waist,
        Leg,
        Foot,
        Toe
    }
}