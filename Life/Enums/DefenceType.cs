﻿namespace Life.Enums
{
    public enum DefenceType
    {
        Human,
        Animal,
        Mechanical,
        Alien
    }
}